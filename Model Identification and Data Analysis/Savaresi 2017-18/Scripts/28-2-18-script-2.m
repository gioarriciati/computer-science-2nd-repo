close all
clear all
clc

%% Consider a white noise WN(1,7) = 1+sqrt(7)*WN(0,1)
e = 1+sqrt(7)*randn(1,100);
m_100 = 1/100*sum(e);

e = 1+sqrt(7)*randn(1,1000);
m_1000 = 1/1000*sum(e);

e = 1+sqrt(7)*randn(1,10000);
m_10000 = 1/10000*sum(e);

e = 1+sqrt(7)*randn(1,100000);
m_100000 = 1/100000*sum(e);

m = [m_100; m_1000; m_10000; m_100000];

% Consistency (N must be high to obtain a good estimate)
mean(e)

% Covariance funtion (tau = 0)
gamma = 1/100000*sum((e-mean(e)).^2);
var(e)

%% Plot covariance function
% The SSP must be zero mean
% Removing the bias
e_tilde = e-mean(e);

for tau = 0:6
    gamma_1(tau+1) = 1/(100000-tau)*sum(e_tilde(1:end-tau).*e_tilde(1+tau:end));
    gamma_2(tau+1) = 1/100000*sum(e_tilde(1:end-tau).*e_tilde(1+tau:end));
end

figure; hold on
plot(-6:6,[-gamma_1(2:end) gamma_1],'*')
plot(-6:6,[-gamma_2(2:end) gamma_2],'o')
legend('Estimator 1','Estimator 2')
xlabel('\tau'); ylabel('\gamma_e(\tau)')
grid on