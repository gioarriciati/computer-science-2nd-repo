clear
clc
close all

%% Initialization
load iris_dataset.mat;
figure();
gplotmatrix(irisInputs');
x = irisInputs(3,:)';
t = irisInputs(4,:)';

figure();
plot(x,t,'bo');

%% Preprocessing
x = zscore(x);
t = zscore(t);
figure();
plot(x,t,'bo');

%% Fit
fit_specifications = fittype( {'1', 'x'}, 'independent', 'x', 'dependent', 't', 'coefficients', {'w0', 'w1'} );
[fitresult, gof] = fit( x, t, fit_specifications);

hold on;
plot(fitresult);

%% Other fit functions
ls_model = fitlm(x,t);

%% By hand regreession
n_sample = length(x);
Phi = [ones(n_sample,1) x];
mpinv = pinv(Phi' * Phi) * Phi';
w = mpinv * t;

hat_t = Phi * w;
bar_t = mean(t);
SSR = sum((t-hat_t).^2);
R_squared = 1 - SSR / sum((t-bar_t).^2);

%% Ridge and lasso
lambda = 10^(-10);
ridge_coeff = ridge(t, Phi, lambda);

[lasso_coeff, lasso_fit] = lasso(Phi, t);
