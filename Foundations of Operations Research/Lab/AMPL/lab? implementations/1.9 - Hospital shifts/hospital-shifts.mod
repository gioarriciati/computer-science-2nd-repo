/*************hospital-shifts.mod*************/
param m;		#number of days
set D:= 1..m;	#set of days

param d{D};	#demand of nurses in day d
param n;	#number of days a nurse work

var x{D} >= 0, integer;	#number of nurses that start their shift in day d

var y{D} >= 0, integer;	#number of nurses that work in day d

minimize totalNumberOfNurses:
	sum{t in D}(x[t]);

subject to demandSatisfy{t in D}:
	y[t] >= d[t];

subject to numberOfNurses{t in D}:
	y[t] = x[t] + sum{d1 in D: d1 < t and (t - d1) <= n or 
			(d1 > t and ((d1 + n) mod m) >= t)}
			( x[d1] );
			
data;

param m:=7;
param n:=5;
param d:=
	1	11,
	2	9,
	3	7,
	4	12,
	5	13,
	6	8,
	7	5;