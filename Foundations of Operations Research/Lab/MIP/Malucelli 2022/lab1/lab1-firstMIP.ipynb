{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6ebf3dcc",
   "metadata": {},
   "source": [
    "# Introduction to the _mip_ Python module\n",
    "\n",
    "The module we'll become most familiar with in this course is the `mip` module, which allows for creating, manipulating, and solving optimization models with linear constraints and integer, binary, or continuous variables. Check the [homepage](https://www.python-mip.com) for full access to the documentation and updates.\n",
    "\n",
    "Suppose you want to model the following problem:\n",
    "$$\n",
    "\\begin{array}{ll}\n",
    "  \\max & x_1 + x_2\\\\\n",
    "  \\textrm{s.t.} & 2 x_1 + x_2 \\le 10\\\\\n",
    "  & x_1, x_2 \\ge 0\n",
    "\\end{array}\n",
    "$$\n",
    "\n",
    "For starters, we import the module `mip` in python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "53f4c8da",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import module\n",
    "import mip"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "86a9bd4d",
   "metadata": {},
   "source": [
    "Next, we create an optimization model `m`. We do so by calling the `mip.Model` *constructor* method. We also create two variables `x1` and `x2` using the `add_var()` method from the optimization model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "4bbf3d62",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create model and add two variables to it\n",
    "m = mip.Model()\n",
    "\n",
    "x1 = m.add_var()\n",
    "x2 = m.add_var()\n",
    "\n",
    "a = 2\n",
    "a = 'ciao ciao'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "f6e83919",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ciao ciao\n"
     ]
    }
   ],
   "source": [
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f44bec0a",
   "metadata": {},
   "source": [
    "We now add the single constraint and the objective. \n",
    "\n",
    "To add the constraint, we use the method `add_constr` from the optimization model.\n",
    "To add the objective function, we set the `objective` attribute of `m`. We use the method `mip.maximize`, to indicate that this is a function to be maximized.\n",
    "\n",
    "For now, since both the constraint and the objective are very simple, we fully write them as algebraic expressions of `x1` and `x2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "3cbd2ab0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Add constraint, set objective function\n",
    "\n",
    "c = m.add_constr(2*x1 + x2 <= 10)\n",
    "m.objective = mip.maximize(x1 + x2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "695a8c81",
   "metadata": {},
   "source": [
    "Finally, we call the method `optimize` to solve the problem and print the value of the optimal solution. For a variable `v` of the module `mip`, its value in the optimal solution is retrieved as the attribute `.x`, for example `v.x`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "9e96094e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Welcome to the CBC MILP Solver \n",
      "Version: devel \n",
      "Build Date: Dec 22 2022\n",
      "Starting solution of the Linear programming problem using Primal Simplex\n",
      "\n",
      "0.0 10.0\n"
     ]
    }
   ],
   "source": [
    "# optimize and print solution\n",
    "\n",
    "m.optimize()\n",
    "\n",
    "print(x1.x, x2.x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "877c07b9",
   "metadata": {},
   "source": [
    "Here's the complete program."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "e42101cd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Coin0506I Presolve 0 (-1) rows, 0 (-2) columns and 0 (-2) elements\n",
      "Clp0000I Optimal - objective value 10\n",
      "Coin0511I After Postsolve, objective 10, infeasibilities - dual 0 (0), primal 0 (0)\n",
      "Clp0032I Optimal objective 10 - 0 iterations time 0.002, Presolve 0.00, Idiot 0.00\n",
      "Starting solution of the Linear programming problem using Primal Simplex\n",
      "\n",
      "var(0) var(1)\n"
     ]
    }
   ],
   "source": [
    "import mip\n",
    "\n",
    "m = mip.Model()\n",
    "\n",
    "x1 = m.add_var()\n",
    "x2 = m.add_var()\n",
    "\n",
    "c = m.add_constr(2*x1 + x2 <= 10)\n",
    "\n",
    "m.objective = mip.maximize(x1 + x2)\n",
    "\n",
    "m.optimize()\n",
    "\n",
    "print(x1, x2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15a48a9a",
   "metadata": {},
   "source": [
    "# A slightly more advanced example\n",
    "\n",
    "Let us now consider a slightly more complicated example: formulating and solving a knapsack problem.\n",
    "\n",
    "$$\n",
    "\\begin{array}{lll}\n",
    "\\max & 3 x_1 + 4 x_2 + 7 x_3 + 5 x_4\\\\\n",
    "\\textrm{s.t.} & 4 x_1 + 5 x_2 + 6 x_3 + 4 x_4 \\le 13\\\\\n",
    "              & x_1, x_2, x_3, x_4 \\in \\{0,1\\}\n",
    "\\end{array}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84b3b82a",
   "metadata": {},
   "source": [
    "For starters, we import the module and define the data used in this model.\n",
    "\n",
    "Next, we create an optimization model with the `mip.Model` *constructor* method. \n",
    "\n",
    "We also add four variables using a list, and call that list `x`. Note that we are using a so-called _list comprehension_ to create variables, i.e., we put a `for` construct _inside_ the list in order to create as many list elements as there are numbers in `range(4)`. As you may have gathered from previous cells, `range(4)` is the set of numbers `0, 1, 2, 3`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "b160fef0",
   "metadata": {},
   "outputs": [],
   "source": [
    "import mip\n",
    "\n",
    "values = [3, 4, 7, 5]\n",
    "weight = [4, 5, 6, 4]\n",
    "max_weight = 13\n",
    "\n",
    "n = len(values)\n",
    "N = range(n)\n",
    "\n",
    "m = mip.Model()\n",
    "\n",
    "x = [m.add_var(var_type=mip.BINARY) for i in N]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1745988d",
   "metadata": {},
   "source": [
    "We now add the single constraint and the objective. In order to create the sum $\\sum_i w_i x_i$, the method `mip.xsum` houls be used. As an argument, one again uses a `for` construct inside the `xsum` argument. The expression\n",
    "\n",
    "```python\n",
    "weight[i] * x[i] for i in range(4)\n",
    "```\n",
    "\n",
    "generates all products $w_ix_i$ for all $i\\in \\{0,1,2,3\\}$ (I know it might be tricky for many to get used to the idea that indices begin at zero in Python, but this will come in handy in the future). This expression is then wrapped inside a `mip.xsum`, which is constrained to be lesser than or equal to `max_weight`. This is the constraint. It is added to the model with the `+=` operator, which is common in Python and other languages such as C/C++ or Java; `a += b` means \"add `b` to `a` and store the result in `a`\".\n",
    "\n",
    "The objective function is a similar `mip.xsum` construction, this time with `value[i]` instead of `weight[i]` for coefficients. It is assigned as the model's objective function with the method `mip.maximize`, to indicate that this is obviously a function to be maximized."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "9e7dbd4a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Add constraint, set objective\n",
    "c = m.add_constr(mip.xsum(weight[i] * x[i] for i in N) <= max_weight)\n",
    "m.objective = mip.maximize(mip.xsum(values[i] * x[i] for i in N))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68e232bd",
   "metadata": {},
   "source": [
    "Finally, we call the method `optimize` to solve the problem and print the value of the optimal solution. For a variable `v` of the module `mip`, its value in the optimal solution is retrieved as the attribute `.x`, for example `v.x`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "75de3989",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Optimize and print solution\n",
    "m.optimize()\n",
    "\n",
    "print([x[i].x for i in N])\n",
    "print([v.x for v in x])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba383225",
   "metadata": {},
   "source": [
    "Complete parametric model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "59ae2e96",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Coin0506I Presolve 0 (-1) rows, 0 (-2) columns and 0 (-2) elements\n",
      "Clp0000I Optimal - objective value 10\n",
      "Coin0511I After Postsolve, objective 10, infeasibilities - dual 0 (0), primal 0 (0)\n",
      "Clp0032I Optimal objective 10 - 0 iterations time 0.002, Presolve 0.00, Idiot 0.00\n",
      "Starting solution of the Linear programming relaxation problem using Primal Simplex\n",
      "\n",
      "Coin0506I Presolve 1 (0) rows, 4 (0) columns and 4 (0) elements\n",
      "Clp1000I sum of infeasibilities 0 - average 0, 4 fixed columns\n",
      "Coin0506I Presolve 0 (-1) rows, 0 (-4) columns and 0 (-4) elements\n",
      "Clp0000I Optimal - objective value -0\n",
      "Clp0000I Optimal - objective value -0\n",
      "Coin0511I After Postsolve, objective 0, infeasibilities - dual 0 (0), primal 0 (0)\n",
      "Clp0006I 0  Obj -0 Dual inf 23.799996 (4)\n",
      "Clp0000I Optimal - objective value 14.4\n",
      "Clp0000I Optimal - objective value 14.4\n",
      "Clp0000I Optimal - objective value 14.4\n",
      "Clp0032I Optimal objective 14.4 - 0 iterations time 0.002, Idiot 0.00\n",
      "\n",
      "Starting MIP optimization\n",
      "[1.0, 1.0, 0.0, 1.0]\n",
      "12.0\n"
     ]
    }
   ],
   "source": [
    "values = [3, 4, 7, 5]\n",
    "weight = [4, 5, 6, 4]\n",
    "max_weight = 13\n",
    "\n",
    "n = len(values)\n",
    "N = range(n)\n",
    "\n",
    "import mip\n",
    "\n",
    "m = mip.Model()\n",
    "\n",
    "x = [m.add_var(var_type=mip.BINARY) for i in N]\n",
    "\n",
    "c = m.add_constr(mip.xsum(weight[i] * x[i] for i in N) <= max_weight)\n",
    "\n",
    "m.objective = mip.maximize(mip.xsum(values[i] * x[i] for i in N))\n",
    "\n",
    "m.optimize()\n",
    "\n",
    "print([x[i].x for i in N])\n",
    "print(m.objective_value)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7e303f7",
   "metadata": {},
   "source": [
    "## Miscellanea and troubleshooting\n",
    "\n",
    "After this first MIP model it's time to say something more about Python.\n",
    "\n",
    "### Re-running code on Jupyter notebooks\n",
    "Code on Jupyter notebooks is fed into Python one cell at a time. If the notebook is written correctly, you should be able to click into the first cell, then just do a `shift`+`enter` through the last cell without any error.\n",
    "\n",
    "You are also able to re-run any cell multiple times, in any sequence you want. However, be aware that Python sees a sequence of cells it is given, and does not know whether an instruction should be undone or not. Therefore, once a cell is run, its results are _persistent_, at least until we reset them. One big red button is the __Restart__ command under the _Kernel_ tab in the menu: it clears all memory of whatever was done in the cell so far (though obviously not file operations). Later in this notebook we show an example of the trouble persistence can cause.\n",
    "\n",
    "### Indentation\n",
    "Indentation is crucial: in a `for` loop, an `if` block, or a function definition, the inner part __MUST__ be indented consistently. Python will throw an error in the following cases:\n",
    "\n",
    "```python\n",
    "for i in [1,2,3]:\n",
    "print(i)\n",
    "```\n",
    "Here the `print` instruction should be indented by at least one space.\n",
    "```python\n",
    "if i==4:\n",
    "    print('i is 4')\n",
    "  print('deal with it')\n",
    "```\n",
    "Here indentation is inconsistent.\n",
    "```python\n",
    "def myfunction(a):\n",
    "return a**4 + 5*a**3\n",
    "```\n",
    "Same as the first incorrect example. The correct way to write these examples is as follows:\n",
    "```python\n",
    "for i in [1,2,3]:\n",
    "    print(i)\n",
    "\n",
    "if i==4:\n",
    "    print('i is 4')\n",
    "    print('deal with it')\n",
    "\n",
    "def myfunction(a):\n",
    "    return a**4 + 5*a**3\n",
    "```\n",
    "The suggested indentation is 4 characters.\n",
    "\n",
    "### Assignment vs. equality\n",
    "The sign `=` is for _assignment_, while `==` is for checking equality of two expressions. You can write `if a == 4` but not `if a = 4`. Also, writing the statement `a = 4` is correct, and so is `a == 4`; however, the latter has no effect (apart from returning `True` or `False` on the Python command line).\n",
    "\n",
    "### Semicolons, be gone!\n",
    "You may have noticed that Python doesn't require semicolons (`;`) at the end of each instruction, as other languages like C, C++, Java, AMPL do. This makes for more readable and prettier code, but indentation is enforced with this in mind.\n",
    "\n",
    "### Writing a statement on multiple lines\n",
    "Related to the last point: conditions can be split on multiple lines as long as a `\\` is added at the end of all but the last one, for example:\n",
    "```python\n",
    "if i==3 or \\\n",
    "   i==4:\n",
    "    print('i is not 5')\n",
    "```\n",
    "But the `\\` is not necessary if there is an unclosed parenthesis, for instance:\n",
    "```python\n",
    "if (i==3 or i==5 or\n",
    "    i==7):\n",
    "    print('i is prime')\n",
    "```\n",
    "### If you're feeling a bit masochistic...\n",
    "A good way to check if your Python program was written according to the standard is to run the `flake8` module on it. Just run `flake8 myprogram.py` and check all the errors it throws (there are usually a ton)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1f4ca767",
   "metadata": {},
   "source": [
    "## Persistence and debugging in Jupyer notebooks\n",
    "\n",
    "Suppose you want to model the following problem:\n",
    "$$\n",
    "\\begin{array}{ll}\n",
    "  \\max & x_1 + x_2\\\\\n",
    "  \\textrm{s.t.} & 2 x_1 + x_2 \\le 10\\\\\n",
    "  & x_1, x_2 \\ge 0\n",
    "\\end{array}\n",
    "$$\n",
    "Let's write the model using `mip`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "ced94229",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "maxSavedSolutions was changed from 1 to 10\n",
      "Continuous objective value is 14.4 - 8.5e-05 seconds\n",
      "Cgl0004I processed model has 1 rows, 4 columns (4 integer (4 of which binary)) and 4 elements\n",
      "Coin3009W Conflict graph built in 0.000 seconds, density: 11.111%\n",
      "Cgl0015I Clique Strengthening extended 0 cliques, 0 were dominated\n",
      "Cbc0045I Cutoff increment increased from 0.0001 to 0.9999\n",
      "Cbc0038I Initial state - 1 integers unsatisfied sum - 0.4\n",
      "Cbc0038I Pass   1: suminf.    0.33333 (1) obj. 13.6667 iterations 1\n",
      "Cbc0038I Solution found of 9\n",
      "Cbc0038I Rounding solution of 12 is better than previous of 9\n",
      "Cbc0038I Before mini branch and bound, 2 integers at bound fixed and 0 continuous\n",
      "Cbc0038I Full problem 1 rows 4 columns, reduced to 0 rows 0 columns\n",
      "Cbc0038I Mini branch and bound did not improve solution (0.00 seconds)\n",
      "Cbc0038I Round again with cutoff of 13.1399\n",
      "Cbc0038I Reduced cost fixing fixed 2 variables on major pass 2\n",
      "Cbc0038I Pass   2: suminf.    0.40000 (1) obj. 14.4 iterations 1\n",
      "Cbc0038I Pass   3: suminf.    0.28498 (1) obj. 13.1399 iterations 1\n",
      "Cbc0038I Pass   4: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass   5: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass   6: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass   7: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass   8: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass   9: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  10: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  11: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  12: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  13: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  14: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  15: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  16: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  17: suminf.    0.25000 (1) obj. 14.25 iterations 2\n",
      "Cbc0038I Pass  18: suminf.    0.28498 (1) obj. 13.1399 iterations 2\n",
      "Cbc0038I Pass  19: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  20: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  21: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  22: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  23: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  24: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  25: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  26: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  27: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  28: suminf.    0.28498 (1) obj. 13.1399 iterations 0\n",
      "Cbc0038I Pass  29: suminf.    0.25000 (1) obj. 14.25 iterations 2\n",
      "Cbc0038I Pass  30: suminf.    0.25000 (1) obj. 14.25 iterations 0\n",
      "Cbc0038I Pass  31: suminf.    0.25000 (1) obj. 14.25 iterations 0\n",
      "Cbc0038I No solution found this major pass\n",
      "Cbc0038I Before mini branch and bound, 2 integers at bound fixed and 0 continuous\n",
      "Cbc0038I Mini branch and bound did not improve solution (0.01 seconds)\n",
      "Cbc0038I After 0.01 seconds - Feasibility pump exiting with objective of 12 - took 0.00 seconds\n",
      "Cbc0012I Integer solution of 12 found by feasibility pump after 0 iterations and 0 nodes (0.01 seconds)\n",
      "Cbc0038I Full problem 1 rows 4 columns, reduced to 1 rows 2 columns\n",
      "Cbc0001I Search completed - best objective 12, took 0 iterations and 0 nodes (0.01 seconds)\n",
      "Cbc0035I Maximum depth 0, 2 variables fixed on reduced cost\n",
      "Cuts at root node changed objective from 14.4 to 12\n",
      "Probing was tried 0 times and created 0 cuts of which 0 were active after adding rounds of cuts (0 seconds)\n",
      "Gomory was tried 0 times and created 0 cuts of which 0 were active after adding rounds of cuts (0 seconds)\n",
      "Knapsack was tried 0 times and created 0 cuts of which 0 were active after adding rounds of cuts (0 seconds)\n",
      "Clique was tried 0 times and created 0 cuts of which 0 were active after adding rounds of cuts (0 seconds)\n",
      "OddWheel was tried 0 times and created 0 cuts of which 0 were active after adding rounds of cuts (0 seconds)\n",
      "MixedIntegerRounding2 was tried 0 times and created 0 cuts of which 0 were active after adding rounds of cuts (0 seconds)\n",
      "FlowCover was tried 0 times and created 0 cuts of which 0 were active after adding rounds of cuts (0 seconds)\n",
      "TwoMirCuts was tried 0 times and created 0 cuts of which 0 were active after adding rounds of cuts (0 seconds)\n",
      "ZeroHalf was tried 0 times and created 0 cuts of which 0 were active after adding rounds of cuts (0 seconds)\n",
      "\n",
      "Result - Optimal solution found\n",
      "Objective value:                12\n",
      "Enumerated nodes:               0\n",
      "Total iterations:               0\n",
      "Time (CPU seconds):             0.004332\n",
      "Time (Wallclock seconds):       0.00704217\n",
      "Total time (CPU seconds):       0.004516   (Wallclock seconds):       0.00734997\n",
      "Starting solution of the Linear programming relaxation problem using Primal Simplex\n",
      "\n",
      "Coin0506I Presolve 0 (-1) rows, 0 (-2) columns and 0 (-2) elements\n",
      "Clp0000I Optimal - objective value 10\n",
      "Coin0511I After Postsolve, objective 10, infeasibilities - dual 0 (0), primal 0 (0)\n",
      "Clp0032I Optimal objective 10 - 0 iterations time 0.002, Presolve 0.00, Idiot 0.00\n",
      "\n",
      "Starting MIP optimization\n",
      "[0.0, 10.0]\n"
     ]
    }
   ],
   "source": [
    "# Copy full model here\n",
    "c = 10\n",
    "b = [2, 1]\n",
    "n = len(b)\n",
    "N = range(n)\n",
    "\n",
    "import mip\n",
    "\n",
    "m = mip.Model()\n",
    "\n",
    "x = [m.add_var(var_type=mip.INTEGER) for i in N]\n",
    "\n",
    "m.add_constr(mip.xsum(x[i]*b[i] for i in N) <= c)\n",
    "\n",
    "m.objective = mip.maximize(mip.xsum(x[i] for i in N))\n",
    "\n",
    "m.optimize()\n",
    "\n",
    "print([x[i].x for i in N])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d965383",
   "metadata": {},
   "source": [
    "Suppose now we want to relax the constraint, for instance change the right-hand side to 20:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "b349ade0",
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'm' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "Cell \u001b[0;32mIn[1], line 3\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[39m# Add relaxed constraints (e.g. with <= 20 instead of <= 10)\u001b[39;00m\n\u001b[0;32m----> 3\u001b[0m m\u001b[39m.\u001b[39madd_constr(mip\u001b[39m.\u001b[39mxsum(x[i]\u001b[39m*\u001b[39mb[i] \u001b[39mfor\u001b[39;00m i \u001b[39min\u001b[39;00m N) \u001b[39m<\u001b[39m\u001b[39m=\u001b[39m \u001b[39m20\u001b[39m)\n\u001b[1;32m      5\u001b[0m \u001b[39m# Re-optimize and print the solution.\u001b[39;00m\n\u001b[1;32m      7\u001b[0m m\u001b[39m.\u001b[39moptimize()\n",
      "\u001b[0;31mNameError\u001b[0m: name 'm' is not defined"
     ]
    }
   ],
   "source": [
    "# Add relaxed constraints (e.g. with <= 20 instead of <= 10)\n",
    "\n",
    "m.add_constr(mip.xsum(x[i]*b[i] for i in N) <= 20)\n",
    "\n",
    "# Re-optimize and print the solution.\n",
    "\n",
    "m.optimize()\n",
    "\n",
    "print('solution:', x1.x, ',', x2.x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1882dd97",
   "metadata": {},
   "source": [
    "The solution is the same even though we relaxed the problem. Why? Well, the problem has two constraints: the one we added in the first cell (which is the more restrictive one) and the last constraint. If we want to relax a problem or change it otherwise, we should modify the cell it is contained in."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.0"
  },
  "vscode": {
   "interpreter": {
    "hash": "5c7b89af1651d0b8571dde13640ecdccf7d5a6204171d6ab33e7c296e100e08a"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
