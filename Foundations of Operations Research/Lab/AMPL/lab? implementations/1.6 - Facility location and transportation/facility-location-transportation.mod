/**********facility-location-transportation.mod**********/

param n;			#number of clients
set J:= 1..n;		#set of clients
param d{J};			#demand of client j

param m;			#number of candidate sites
set I:= 1..m;		#set of candidate sites
param f{I};			#cost for opening warehouse in candidate site i
param b{I};			#maximum capacity of warehouse if opened in candidate site i

param c{I,J};		#unit transportation cost from warehouse i to client j
param q{I,J};		#maximum quantity to be transported from warehouse i to client j

var x{I} binary;	#x[i] = 1 if a warehouse is opened at candidate site i
var t{I,J}>=0;		#t[t,j] = quantity of product transported from warehouse at candidate site i
					#to client j

#Total cost: opening of warehouses + transportation costs
minimize totalCost:
	sum{i in I}(x[i]*f[i]) + sum{i in I,j in J}( t[i,j]*c[i,j] );
	
subject to capacity{i in I}:
	sum{j in J}( t[i,j] ) <= b[i];
	
subject to activation{i in I, j in J}:
	t[i,j] <= x[i]*b[i];

subject to maxTransportableQuantity{i in I, j in J}:
	t[i,j] <= q[i,j];

subject to demand{j in J}:
	sum{i in I}( t[i,j] )>= d[j];

data;

param n:=4;
param d:=
	1	3,
	2	5,
	3	6,
	4	2;
	
param m:= 3;
param f:=
	1	10,
	2	15,
	3	11;
	
param b :=
	1	5,
	2	7,
	3	9;
	
param c:=
	1	1	2,
	1	2	3,
	1	3	4,
	1	4	1,
	2	1	5,
	2	2	7,
	2	3	4,
	2	4	2,
	3	1	6,
	3	2	5,
	3	3	4,
	3	4	1;
	
param q:=
	1	1	4,
	1	2	3,
	1	3	6,
	1	4	7,
	2	1	3,
	2	2	9,
	2	3	10,
	2	4	4,
	3	1	8,
	3	2	9,
	3	3	15,
	3	4	3;
