; Example: the state monad seen in class
;; translated directly into Scheme
;; MPradella, MMXIII

#lang racket

(define (return x)
  (lambda (t)
    (cons t x)))

(define (>>= f g)
  (lambda (oldstate)
    (let* ((fappl    (f oldstate))
           (newstate (car fappl))
           (val      (cdr fappl)))
      ((g val) newstate))))

(define (>> m k) ; standard definition
  (>>= m (lambda (x) k)))

(define (getState)
  (lambda (state)
    (cons state state)))

(define (putState new)
  (lambda (x)
    (cons new '())))


;; some of the examples seen in class:

(define (esm)
  (>>= (return 5)
       (lambda (x)
         (return (+ x 1)))))

(displayln ((esm) 333))

(define (esm2)
  (>>= (getState)
       (lambda (x)
         (>> (putState (+ x 1))
             (>> (getState)
                 (return x))))))

(displayln ((esm2) 333))

;; State monad and trees. 
;; For simplicity: Leaf = Atom; Branch = Cons

(define (mapTreeM f tree)
  (if (not (cons? tree))
      (>>= (f tree)
           (lambda (b)
             (return b)))
      (let ((lhs (car tree))
            (rhs (cdr tree)))
        (>>= (mapTreeM f lhs)
             (lambda (lhs1)
               (>>= (mapTreeM f rhs)
                    (lambda (rhs1)
                      (return (cons lhs1 rhs1)))))))))

(define (runStateM f st)
  (cdr (f st)))

(define (numberTree tree)
  (define (number v)
    (>>= (getState)
         (lambda (curr)
           (>> (putState (+ curr 1))
               (return (cons v curr))))))
  (mapTreeM number tree))

(define testTree (cons 
                  (cons 'a 
                        (cons 'b 'c))
                  (cons 'd 'e)))

(displayln (runStateM (numberTree testTree) 1))
  