/********portfolio.mod**********/

param C;	#capital of the bank
param n;
set J:= 1..n;

param r{J};		#revenue percentage of j-th stock
param f{J};		#risk factor of j-th stock


var x{J}>= 0;		#amount of money used to buy j-th stock

maximize profit:
	x[1]*r[1] + x[2]*r[2];

subject to balancing:
	x[2] <= 2*x[1];
	
subject to minimalInvestmentX1:
	x[1] >= C/6;

subject to halfRiskFree:
	x[1]*f[1] + x[2]*f[2] <= C/2;
	
subject to maxTotalInvestment:
	x[1] + x[2] <= C;
	
data;

param C:=10;
param n:=2;

param r:= 
1	0.15,
2	0.25;

param f:=
1	0.3333,
2	1;
