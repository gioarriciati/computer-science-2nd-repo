clear
clc
close all

%% Automatic
load iris_dataset.mat

gplotmatrix(irisInputs');
x = zscore(irisInputs(3,:)');
t = zscore(irisInputs(4,:)');

GPmodel = fitrgp(x, t, 'KernelFunction', 'squaredexponential');

theta = GPmodel.KernelInformation.KernelParameters
sigma = GPmodel.Sigma

plotGp(GPmodel);

%% Signal Standard Deviation
theta1 = [theta(1) 10 * theta(2)];
GPmodel = fitrgp(x, t, 'KernelParameters', theta1, 'Sigma', sigma, 'FitMethod', 'none');
plotGp(GPmodel);

theta2 = [theta(1) 0.1 * theta(2)];
GPmodel = fitrgp(x, t, 'KernelParameters', theta2, 'Sigma', sigma, 'FitMethod', 'none');
plotGp(GPmodel);

%% Bandwidth

theta1 = [10 * theta(1) theta(2)];
GPmodel = fitrgp(x, t, 'KernelParameters', theta1, 'Sigma', sigma, 'FitMethod', 'none');
plotGp(GPmodel);

theta2 = [0.1 * theta(1) theta(2)];
GPmodel = fitrgp(x, t, 'KernelParameters', theta2, 'Sigma', sigma, 'FitMethod', 'none');
plotGp(GPmodel);

%% Different noise

sigma1 = 10 * sigma;
GPmodel = fitrgp(x, t, 'KernelParameters', theta, 'Sigma', sigma1, 'FitMethod', 'none');
plotGp(GPmodel);

sigma2 = 0.1 * sigma;
GPmodel = fitrgp(x, t, 'KernelParameters', theta2, 'Sigma', sigma2, 'FitMethod', 'none');
plotGp(GPmodel);

%% User defined kernel function
hand = @gaussKer;

GPmodel = fitrgp(x, t, 'KernelFunction', hand, 'KernelParameters', [1 1], 'Sigma', 1);
plotGp(GPmodel);

exp(GPmodel.KernelInformation.KernelParameters)

%% GP by hand

theta0 = log(theta);
sigma0 = sigma;
n_train = 150;
n_new = 100;

x_new = linspace(min(x),max(x),n_new)';

K = gaussKer(x, x, theta0) + sigma0 * eye(n_train);
for ii = 1:n_new
    Kstar = gaussKer(x_new(ii), x, theta0);
    Kstarstar = gaussKer(x_new(ii), x_new(ii), theta0) + sigma0^2;

    y_new(ii) = Kstar * pinv(K) * t;
    y_var(ii) = Kstarstar - Kstar * pinv(K) * Kstar';

end

y_sup = y_new + 2 * sqrt(y_var);
y_inf = y_new - 2 * sqrt(y_var);
figure()
hold on;
plot(x, t, 'r.');
plot(x_new, y_new, 'b', 'LineWidth', 2);
h1 = fill([x_new' x_new(end:-1:1)'],[y_sup y_inf(end:-1:1)],'k');
alpha(h1,.2);
axis tight;
