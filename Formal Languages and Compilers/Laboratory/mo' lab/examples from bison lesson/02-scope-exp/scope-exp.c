#include "scope-exp.h"
#include "scope-exp.parser.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int yychar;
extern char *yytext;
extern FILE *yyin;

struct var_t {
  struct var_t *next;
  const char *name;
  int value;
};

struct scope_t {
  struct scope_t *next;
  struct var_t *vars;
};

struct scope_t *scope_stack = NULL;

void push_scope() {
  struct scope_t *s = malloc(sizeof(struct scope_t));
  s->next = scope_stack;
  s->vars = NULL;
  scope_stack = s;
}

void pop_scope() {
  assert(scope_stack);
  struct scope_t *s = scope_stack;
  scope_stack = s->next;

  struct var_t *v;
  while ((v = s->vars)) {
    s->vars = v->next;
    free(v);
  }
  free(s);
}

bool has_value(struct scope_t *s, const char *id, struct expr_t *e) {
  for (struct var_t *v = s->vars; v; v = v->next) {
    if (strcmp(v->name, id) == 0) {
      if (e) {
        e->value = v->value;
        e->poison = 0;
      }
      return true;
    }
  }
  return false;
}

struct expr_t get_value(const char *id) {
  struct expr_t e;
  for (struct scope_t *s = scope_stack; s; s = s->next) {
    if (has_value(s, id, &e))
      return e;
  }

  fprintf(stderr, "Unknown variable '%s'.\n", id);

  e.poison = 1;
  return e;
}

void decl_var(const char *id, int value) {
  assert(scope_stack && "Missing scope!");
  if (has_value(scope_stack, id, NULL)) {
    fprintf(stderr, "Variable '%s' already declared "
                    "in the current scope!\n", id);
    return;
  }

  struct var_t *v = malloc(sizeof(struct var_t));
  v->name = id;
  v->value = value;
  v->next = scope_stack->vars;
  scope_stack->vars = v;
}

void decl_var_input(const char *id) {
  assert(scope_stack && "Missing scope!");
  if (has_value(scope_stack, id, NULL)) {
    fprintf(stderr, "Variable '%s' already declared "
                    "in the current scope!\n", id);
    return;
  }

  struct var_t *v = malloc(sizeof(struct var_t));
  v->name = id;
  v->next = scope_stack->vars;
  scope_stack->vars = v;

  printf("> %s = ", id);
  int n = 0;
  do {
    n = scanf("%d%*c", &(v->value));
    if (n == 0)
      printf("> %s = ", id);
  } while (n == 0);
}

void print_result(struct expr_t *e, int i) {
  if (e->poison)
    printf("$%d = <poison value>\n", i);
  else
    printf("$%d = %d\n", i, e->value);
  printf("------------------------------------\n");
}

void yyerror(const char *msg) {
  if (yychar == LEX_ERR)
    fprintf(stderr, "Lexical error: unkown token '%s'\n", yytext);
  else fprintf(stderr, "%s\n", msg);
}

int main(int argc, char **argv) {
  if (argc < 2) {
    fprintf(stderr, "Missing input file!\n");
  }
  yyin = fopen(argv[1], "r");
  if (yyparse()) {
    fprintf(stderr, "Unable to parse the input!\n");
    return 1;
  }

  fclose(yyin);
  return 0;
}

