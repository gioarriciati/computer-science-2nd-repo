set I;
set J;

param b{I};
param q{J};
param c{J};
param a{I,J};

var x{J} >= 0;

minimize costo:
	sum{j in J} c[j] * x[j];

subject to disponibilita{j in J}:
	x[j] <= q[j];

subject to fabbisogno{i in I}:
	sum{j in J} a[i,j] * x[j] >= b[i];