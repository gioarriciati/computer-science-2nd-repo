clear
clc
close all

colors = ['m' 'g' 'b' 'k'];

R = [0.2 0.3 0.7 0.5];
n_arms = length(R);

%Environment
lab = cell(n_arms,1);
for ii = 1:n_arms
    mathcal_R(ii) = makedist('Binomial', 'p', R(ii));
    lab{ii} = ['a_' num2str(ii)];
end
T = 1000;

%%
% Thompson Sampling
hat_r = zeros(1,n_arms);
f = zeros(1,n_arms);
cum_r = zeros(1,n_arms);

for ii = 1:n_arms
    beta_dist(ii) = makedist('Beta','a',1,'b',1);
end

figure();
ind = zeros(T,1);
rewards = zeros(T,1);

for tt = 1:T
    %Decision
    for ii = 1:n_arms
        hat_r(ii) = beta_dist(ii).random();
    end    
    [~, ind(tt)] = max(hat_r);

    %Plot
    if tt <= 10 || mod(tt, T/20) == 0
        clf;
        for ii = 1:n_arms
            x = 0:0.01:1;
            y = beta_dist(ii).pdf(x);
            plot(x,y,'color',colors(ii));
            hold on;
            hat_y = beta_dist(ii).pdf(hat_r(ii));
            if hat_r(ii) == max(hat_r)
                plot(hat_r(ii), hat_y, 'x', 'color', colors(ii),'markersize',9,'linewidth',4);
            else
                plot(hat_r(ii), hat_y, 'x', 'color', colors(ii));
            end
        end
        %title(['Arm selected ' lab{ind(tt)} ', t = ' num2str(tt)]);
        drawnow();
        pause();
    end
    
    %Reward
    outcome = mathcal_R(ind(tt)).random();
    rewards(tt) = outcome;
    
    %Update statistics
    beta_dist(ind(tt)).a = beta_dist(ind(tt)).a + outcome;
    beta_dist(ind(tt)).b = beta_dist(ind(tt)).b + 1 - outcome;
    
end