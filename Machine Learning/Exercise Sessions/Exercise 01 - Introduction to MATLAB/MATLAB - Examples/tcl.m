clear
clc
close all

%% Sample from different distributions

mu = 4;
sigma = 1;

expo = makedist('exponential','mu', mu);
gaus = makedist('normal','mu', mu, 'sigma', sigma);

n_samples = 10000;
expo_samp = expo.random(n_samples,1);
gaus_samp = gaus.random(n_samples,1);

figure();
histogram(expo_samp,10);
title('Exponential distribution');
%print('expo','-deps');
figure();
histogram(gaus_samp,10);
title('Gaussian distribution');
%print('gaus','-deps');

mean(expo_samp)
mean(gaus_samp)

%% Empirical mean distribution
n_trial = 1000;
n_samples = 10000;

expo_mean = zeros(n_trial,1);
gaus_mean = zeros(n_trial,1);

for i = 1:n_trial
    %Sampling
    expo_samp = expo.random(n_samples,1);
    gaus_samp = gaus.random(n_samples,1);
    
    %Mean computation
    expo_mean(i) = mean(expo_samp);
    gaus_mean(i) = mean(gaus_samp);
end

figure()
subplot(2,1,1);
hystogram_plus(expo_mean, 30, mu, 4 / sqrt(n_samples));
title('Empiric distribution of the sample mean for exponential');
subplot(2,1,2);
hystogram_plus(gaus_mean, 30, mu, 1 / sqrt(n_samples));
title('Empiric distribution of the sample mean for Gaussian');
%print('tcl','-deps');