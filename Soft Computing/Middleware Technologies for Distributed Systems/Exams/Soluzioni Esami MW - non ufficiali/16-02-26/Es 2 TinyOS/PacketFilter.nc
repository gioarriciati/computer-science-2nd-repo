module PacketFilter{
	provides interface Receive;
	provides interface Read<val_t>;
	uses interface Receive;
}

implementation{
	uint16_t lostPackets = 0;
	
	command error_t Read.read(){
		signal readDone(SUCCESS, lostPackets);
		return SUCCESS
	}
	
	event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len){
		if(payload[0] != 255){
			lostPackets++;
		} else {
			signal Receive.receive(msg, payload, len);
		}
	} 
		
}
