%% Ex.2
close all
clear all
clc

% WN(0,4)
e = 2*randn(1,100);

% ARMA(1,1)
y(1) = 0;
for i = 2:100
    y(i) = 0.5*y(i-1)+e(i)+8*e(i-1);
end

figure;
subplot(2,1,1);
plot(e); title('Input e(t)')
subplot(2,1,2); plot(y); title('Output y(t)')

% Operatorial formulation
z = tf('z');
filt = (z+8)/(z-1/2);
y1 = lsim(filt,e);

figure; hold on
plot(y) 
plot(y1)
title('Output y(t)')

%% Ex. 3
close all
clear all
clc

% Process
z = tf('z');
filt = (z^2 + 2*z + 3)/(z^2 - (1/2)*z - 1/4);

figure;
pzplot(filt)

%% Ex. 4
close all
clear all
clc

% WN(0,1)
e = randn(1,500);

% Process
z = tf('z');
filt = z/(z-1);

figure;
plot(lsim(filt,e))
title('Output y(t)')

% WN(0,1)
e = randn(1,50);

% Process
z = tf('z');
filt = (z-0.5)/(z-1.1);

figure;
plot(lsim(filt,e))
title('Output y(t)')

%% Ex. 5
close all
clear all
clc

% WN(0,1)
e = randn(1,500);

% Process
z = tf('z');
f1 = (z+0.5)/(z-0.9);
f2 = (z+0.5)/(z+0.9);

figure;
pzplot(f1,f2)

figure; hold on
plot(lsim(f1,e))
plot(lsim(f2,e))
title('Output y(t)')